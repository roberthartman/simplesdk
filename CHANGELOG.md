# Change Log
All notable changes to this project will be documented in this file.
SimpleSdk adheres to [Semantic Versioning](http://semver.org/).

## [Master](https://github.com/roberthartman/SimpleSdk)
### Added

### Changed

### Removed
