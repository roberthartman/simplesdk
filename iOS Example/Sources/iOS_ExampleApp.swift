//
//  iOS_ExampleApp.swift
//  iOS Example
//
//  Created by Robert Hartman on Aug 9, 2021.
//

import SwiftUI

@main
struct iOS_ExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
