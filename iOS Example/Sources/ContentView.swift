//
//  ContentView.swift
//  iOS Example
//
//  Created by Robert Hartman on Aug 9, 2021.
//

import SwiftUI
import SimpleSdk

struct SwiftUISimpleSdk: UIViewRepresentable {
    func makeUIView(context: Context) -> UIView {
        return SimpleSdk()
    }

    func updateUIView(_ uiView: UIView, context: Context) {
    }
}

struct ContentView: View {
    var body: some View {
        VStack(alignment: .center) {
            SwiftUISimpleSdk()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
