import XCTest
@testable import SimpleSdk

final class SimpleSdkTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SimpleSdk().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
