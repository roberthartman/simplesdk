import XCTest

import SimpleSdkTests

var tests = [XCTestCaseEntry]()
tests += SimpleSdkTests.allTests()
XCTMain(tests)
